package com.ims.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddBillPage {

	
	WebDriver ldriver;

	 public	AddBillPage(WebDriver rdriver) {
			ldriver = rdriver;
			PageFactory.initElements(rdriver, this);

		}
	 
	 @FindBy(css= "button.nav-arrow.right-direction-arrow > svg")
	 @CacheLookup
	 WebElement expandMenu_btn;
	 @FindBy(xpath= "//div[@id='root']/div/div[2]/div/div/ul/li[8]/a/span")
	 @CacheLookup
	 WebElement bill_MenuItem;
	 @FindBy(xpath= "//div[@id='root']/div/div[2]/div[2]/div/div/button")
	 @CacheLookup
	 WebElement new_Bill_btn;
	 
	 @FindBy(xpath= "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]")
	 @CacheLookup
	 WebElement vendor_list;
	 
	 @FindBy(id= "react-select-4-option-0")
	 @CacheLookup
	 WebElement selectVendor;
	 
	 @FindBy(xpath= "//div[@id='root']/div/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div/div/form/div[3]/table/tbody/tr/td/div/div/div/div/div")
	 @CacheLookup
	 WebElement itemsList;
	 
	 @FindBy(xpath= "//div[contains(text(),'Red Roses')]")
	 @CacheLookup
	 WebElement selectItem;
	 
	 @FindBy(xpath= "//button[@type='submit']")
	 @CacheLookup
	 WebElement saveBill_btn;
	 

		public void clickexpandMenu_btn() {
			expandMenu_btn.click();

		}
		public void clickbill_MenuItem() {
			bill_MenuItem.click();

		}
		public void clicknew_Bill_btn() {
			new_Bill_btn.click();

		}
		public void clicvendor_list() {
			vendor_list.click();

		}
		public void clicselectVendor() {
			selectVendor.click();

		}
		public void clickitemsList() {
			itemsList.click();

		}
		public void clickselectItem() {
			selectItem.click();

		}
		public void clickSaveBill() {
			saveBill_btn.click();

		}
	
}
