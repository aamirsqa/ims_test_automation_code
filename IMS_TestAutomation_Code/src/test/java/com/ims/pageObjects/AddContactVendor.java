package com.ims.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddContactVendor {
	
	
	WebDriver ldriver;

	public AddContactVendor(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);

	}

	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[1]/button[1]")
	@CacheLookup
	WebElement Add_NewContact_button;

	@FindBy(xpath = "//label[contains(text(),'Vendor')]")
	@CacheLookup
	WebElement vendor;

	@FindBy(xpath = "//input[@id='firstName']")
	@CacheLookup
	WebElement firstName;

	@FindBy(xpath = "//button[contains(text(),'Save')]")
	@CacheLookup
	WebElement saveContact;

	public void clickAdd_NewContact_button() {
		Add_NewContact_button.click();

	}

	public void clickVendor() {

		vendor.click();
		;
	}

	public WebElement getFirstName() {

		return firstName;
	}

	public void clickSaveContact() {

		saveContact.click();
	}


}
