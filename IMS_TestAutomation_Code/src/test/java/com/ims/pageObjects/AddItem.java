package com.ims.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ims.testcases.BaseClass;

public class AddItem extends BaseClass {
	
	
	WebDriver ldriver;

	 public	AddItem(WebDriver rdriver) {
			ldriver = rdriver;
			PageFactory.initElements(rdriver, this);

		}
	 
	 @FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[1]/button[1]")
		@CacheLookup
		WebElement Add_Item_button;
	 
	 @FindBy(xpath = "//input[@id='name']")
		@CacheLookup
		WebElement item_Name;
	
	 @FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[3]/div[1]/input[1]")
		@CacheLookup
		WebElement item_Unit;
	
	 
	 @FindBy(xpath = "//button[contains(text(),'Generate')]")
		@CacheLookup
		WebElement gen_item_SKU;
	
	 @FindBy(xpath = "//input[@id='salesUnitPrice']")
		@CacheLookup
		WebElement salesUnitPrice;
	 
	 @FindBy(xpath = "//input[@id='purchaseUnitPrice']")
		@CacheLookup
		WebElement purchaseUnitPrice;
	 
	 @FindBy(xpath = "//input[@id='openingStockValue']\r\n")
		@CacheLookup
		WebElement openingStockValue;
	
	 @FindBy(xpath = "//button[contains(text(),'Save')]")
		@CacheLookup
		WebElement saveIteam;
	
	 
		public void clickAddItemButton() {
			Add_Item_button.click();

		}

		public WebElement getItemName() {
			
			return item_Name;
		}
		public WebElement getitem_Unit() {
			
			return item_Unit;
		}


	public void clickGenSKU() {
			
		gen_item_SKU.click();
		}
	public WebElement getSaleUnitPrice() {
		
		return salesUnitPrice;
	}
	public WebElement getpurchaseUnitPrice() {
		
		return purchaseUnitPrice;
	}
	public WebElement getopeningStockValue() {
		
		return openingStockValue;
	}
	public void clickSaveIteam() {
		
		saveIteam.click();
	}
}
