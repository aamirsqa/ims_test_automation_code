package com.ims.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddOrgPage {
	
	
	
	WebDriver ldriver;

	public AddOrgPage(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);

	}
	
	@FindBy(css = "button.nav-arrow.right-direction-arrow > svg")
	@CacheLookup
	WebElement load_Page;
 
	@FindBy(css = "//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/ul[1]/li[11]/a[1]/span[1]")
	@CacheLookup
	WebElement load_OrgModule;

	@FindBy(xpath = "//a[contains(text(),'Add Organization')]")
	@CacheLookup
	WebElement Add_NewOrg;

	@FindBy(xpath = "//input[@id='organizationName']")
	@CacheLookup
	WebElement org_Name;
	
	
	 @FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/div[1]/*[1]")
		@CacheLookup
		WebElement load_OrgList;
	 

	@FindBy(xpath = "//div[contains(text(),'Technology')]")
	@CacheLookup
	WebElement setOrgType;

	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[5]/div[1]/div[1]/div[2]/div[1]/*[1]")
	@CacheLookup
	WebElement fYearList;
	
	@FindBy(xpath = "//div[contains(text(),'February - January')]")
	@CacheLookup
	WebElement fYear;
	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[7]/div[1]/div[1]/div[2]/div[1]/*[1]")
	@CacheLookup
	WebElement baseCurrencyList;
	
	@FindBy(xpath = "//div[contains(text(),'US Dollar - USD')]")
	@CacheLookup
	WebElement setBaseCurrency;

	@FindBy(xpath = "//input[@id='phone']")
	@CacheLookup
	WebElement phoneNumber;
	
	@FindBy(xpath = "//button[contains(text(),'Save')]")
	@CacheLookup
	WebElement saveOrg;
	
	
	
	public void load_Page() {

		load_Page.click();
		
	}
	
	public void load_OrgModulee() {

		load_OrgModule.click();
		
	}
	public void clickAdd_NewOrg_button() {
		Add_NewOrg.click();

	}



	public WebElement getorg_Name() {

		return org_Name;
	}

	public void clickload_OrgTypeList() {

		load_OrgList.click();
	}


	public void setOrgType() {

		setOrgType.click();;
	}

	public void clickfYearList() {

		fYearList.click();
	}
	public void clickfYear() {

		fYear.click();
	}

	public void clickbaseCurrencyList() {

		baseCurrencyList.click();
	}
	
	public void clicksetBaseCurrency() {

		setBaseCurrency.click();
	}
	public WebElement setphoneNumber() {

	   return phoneNumber;
	}
	
	public void clicksaveOrg() {

		saveOrg.click();
	}
	

}
