package com.ims.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddPackagePage {

	

	WebDriver ldriver;

	 public	AddPackagePage(WebDriver rdriver) {
			ldriver = rdriver;
			PageFactory.initElements(rdriver, this);

		}
	 
	 @FindBy(css = "button.nav-arrow.right-direction-arrow > svg")
		@CacheLookup
		WebElement load_Page;
	 
	 @FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[1]/button[1]")
		@CacheLookup
		WebElement Add_Package_button;
	 
	 @FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[3]/div[1]/div[1]/div[2]/div[1]/*[1]")
		@CacheLookup
		WebElement salesOrder_List;
	 @FindBy(xpath = "//div[contains(text(),'SO-00000007')]")
		@CacheLookup 
		WebElement salesOrder_No ;
	
	 @FindBy(xpath = "//button[contains(text(),'Save')]")
		@CacheLookup
		WebElement packageSave;
	
	public void load_Page() {
			
			 load_Page.click();
		}

	 
		public void clickAdd_Package_Button() {
			Add_Package_button.click();

		}

		public void getsalesOrder_List() {
			
			salesOrder_List.click();
		}
		
public void getsalesOrder_No() {
			
	salesOrder_No.click();
		}
	public void clickpackageSave_button() {
			
		packageSave.click();
		}
	
	
}
