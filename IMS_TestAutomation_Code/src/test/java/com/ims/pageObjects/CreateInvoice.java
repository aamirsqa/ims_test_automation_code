package com.ims.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class CreateInvoice {
	
	
	WebDriver ldriver;

	 public	CreateInvoice(WebDriver rdriver) {
			ldriver = rdriver;
			PageFactory.initElements(rdriver, this);

		}
	 
	 @FindBy(css = "button.nav-arrow.right-direction-arrow > svg")
		@CacheLookup
		WebElement load_Page;
	 
	 @FindBy(xpath = "//div[@id='root']/div/div[2]/div/div/ul/li[6]/a/span")
		@CacheLookup
		WebElement load_Invoice_Module;
	 
	 @FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[1]/button[1]")
		@CacheLookup
		WebElement Add_Invoice_button;
	 
	 
	 @FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/div[1]/div[1]")
		@CacheLookup
		WebElement customerList;
	 
	 @FindBy(xpath = "//div[contains(text(),'Solutin')]")
		@CacheLookup
		WebElement select_customer;
	 
	 @FindBy(xpath = "//div[@id='root']/div/div[2]/div[2]/div/div[2]/div[2]/div/div[2]/div/div/div/form/div[9]/table/tbody/tr/td/div/div/div/div/div")
		@CacheLookup
		WebElement item_List;
	 
	 @FindBy(xpath = "//div[contains(text(),'Orange Watch')]")
		@CacheLookup
		WebElement select_Item;
	 @FindBy(xpath = "//button[@type='submit']")
		@CacheLookup
		WebElement save_button;
	 
	 public void click_load_Page() {
		 load_Page.click();

		}
	 
		public void clickload_Invoice_Module() {
			load_Invoice_Module.click();

		}
		public void clickAddInvoceButton() {
			Add_Invoice_button.click();

		}
		public void clickcustomerList() {
			customerList.click();

		}
		
		public void clickselect_customer() {
			select_customer.click();

		}
		
			 
		public void load_item_List() {
		
			item_List.click();
			
		}

		public void click_select_Item() {
			
			select_Item.click();
			
		}
		public void click_save_button() {
			
			save_button.click();
			
		}
}
