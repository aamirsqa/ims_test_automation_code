package com.ims.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	WebDriver ldriver;

	public LoginPage(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);

	}

	@FindBy(xpath = "//body/div[@id='mm-0']/div[@id='main-container']/div[2]/div[2]/div[1]/div[1]/div[2]/nav[1]/div[1]/ul[1]/li[4]/a[1]/span[1]")
	@CacheLookup
	WebElement signin_button;

	@FindBy(id = "email")
	@CacheLookup
	WebElement txtUserName;

	@FindBy(id = "password")
	@CacheLookup
	WebElement txtPasswrod;

	@FindBy(xpath = "//button[@class='button--full-width']")
	@CacheLookup
	WebElement loginbutton;

	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[1]/div[3]/div[3]/div[1]/img[1]")
	@CacheLookup
	WebElement orgIcon;

	@FindBy(xpath = "//button[contains(text(),'Sign out')]")
	@CacheLookup
	WebElement logout_btn;

	public void clickSignInButton() {
		signin_button.click();

	}

	public void setUserName(String uname) {

		txtUserName.sendKeys(uname);

	}

	public void setPassword(String password) {

		txtPasswrod.sendKeys(password);

	}

	public void clickLoginButton() {
		loginbutton.click();

	}

	public void clickorgIcon() {
		orgIcon.click();

	}

	public void clicklogout_btn() {
		logout_btn.click();

	}

}
