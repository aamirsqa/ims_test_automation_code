package com.ims.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {

	
	WebDriver ldriver;
	
	public MainPage(WebDriver rdriver)
	{
		
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);
	}
	
	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/ul[1]/li[2]/a[1]")
	@CacheLookup
	WebElement contactModule;
	
	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/ul[1]/li[4]/a[1]/span[1]")
	@CacheLookup
	WebElement salesOrderModule;
	
	@FindBy(xpath = "//span[contains(text(),'Purchase Orders')]")
	@CacheLookup
	WebElement purchaseOrderModule;
	
	@FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[1]/ul[1]/li[6]/a[1]/*[local-name()='svg'][1]/*[name()='path'][1]")
	@CacheLookup
	WebElement invoiceModule;
	
	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/ul[1]/li[5]/a[1]")
	@CacheLookup
	WebElement packageModule;
	
	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/ul[1]/li[11]/a[1]")
	@CacheLookup
	WebElement orgModule;
	
	public void clickInvoceModule() {
		invoiceModule.click();

	}
	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/ul[1]/li[3]/button[1]")
	@CacheLookup
	WebElement itemModule;
	
	
	public void clickItemModule() {
		itemModule.click();

	}
	
	public void clickContactModule() {
		contactModule.click();

	}
	
	public void clicksalesOrderModule() {
		salesOrderModule.click();

	}
	
	public void clickpurchaseOrderModule() {
		purchaseOrderModule.click();

	}
	
	public void clickpackageModule() {
		packageModule.click();

	}
	
	
	public void clickorgModule() {
		orgModule.click();

	}
	
	
	
	
	
}
