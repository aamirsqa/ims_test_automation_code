package com.ims.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PurchaseOrderPage {

	
	WebDriver ldriver;

	public PurchaseOrderPage(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);

	}
	
	@FindBy(css = "button.nav-arrow.right-direction-arrow > svg")
	@CacheLookup
	WebElement load_Page;
 
	@FindBy(css = "//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/ul[1]/li[7]/a[1]/span[1]")
	@CacheLookup
	WebElement PurchaseOrderModule;

	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[1]/button[1]")
	@CacheLookup
	WebElement Add_NewPurchaseOrder;

	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/*[1]")
	@CacheLookup
	WebElement ven_List;
	
	
	 @FindBy(xpath = "//div[contains(text(),'Michle')]")
		@CacheLookup
		WebElement select_vendor;
	 

	@FindBy(xpath = "//tbody/tr[1]/td[1]/div[1]/div[1]/div[1]/div[2]/div[1]/*[1]")
	@CacheLookup
	WebElement getItemList;

	@FindBy(xpath = "//div[contains(text(),'car')]")
	@CacheLookup
	WebElement selectItem;
	
	@FindBy(xpath = "//button[contains(text(),'Save')]")
	@CacheLookup
	WebElement savePO;

	
	
	public void load_Page() {

		load_Page.click();
		
	}
	
	public void load_PurchaseOrderModule() {

		PurchaseOrderModule.click();
		
	}
	public void clickNewPurchaseOrder_button() {
		Add_NewPurchaseOrder.click();

	}



	public void getven_List() {

		ven_List.click();
	}

	public void clickSelectVendor() {

		select_vendor.click();
	}


	public WebElement loadItemList() {

		return getItemList;
	}

	public void clickselectItem() {

		selectItem.click();
	}
	public void clicksavePO() {

		savePO.click();
	}

	
	
}
