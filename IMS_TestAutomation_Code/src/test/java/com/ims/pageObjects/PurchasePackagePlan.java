package com.ims.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PurchasePackagePlan {
	
	
	
	

	WebDriver ldriver;

	public PurchasePackagePlan(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);

	}


	@FindBy(xpath = "//button[contains(text(),'My Account')]")
	@CacheLookup
	WebElement Account_btn;

	@FindBy(xpath = "//button[contains(text(),'Upgrade')]")
	@CacheLookup
	WebElement Upgrade_btn;
	
	
	
	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[3]/div[1]/div[1]/div[3]/button[1]/div[2]/div[1]")
	@CacheLookup
	WebElement PricePlan_btn;
	
	
	 @FindBy(xpath = "//input[@id='View']")
		@CacheLookup
		WebElement Terms_Agree;
	 

	@FindBy(xpath = "//button[contains(text(),'Confirm')]")
	@CacheLookup
	WebElement confirm_btn;

	@FindBy(xpath = "//input[@id='confirmButtonTop']")
	@CacheLookup
	WebElement agree_continue_btn;
	
	@FindBy(xpath = "//input[@id='email']")
	@CacheLookup
	WebElement email_txt;
	
	@FindBy(id = "btnNext")
	@CacheLookup
	WebElement btnNext;
	
	
	@FindBy(id = "password")
	@CacheLookup
	WebElement password_txt;
	
	@FindBy(id = "btnLogin")
	@CacheLookup
	WebElement btnLogin;
	
	
	public void Account_btn() {

		Account_btn.click();
		
	}
	
	public void Upgrade_btn() {

		Upgrade_btn.click();
		
	}
	
	
	public void PricePlan_btn() {

		PricePlan_btn.click();
		
	}
	public void Terms_Agree() {
		Terms_Agree.click();

	}



	public void confirm_btn() {

		confirm_btn.click();
	}
	public void enterEmail() {

		email_txt.sendKeys("sb-r1pyf3328719@business.example.com");
	}
	
	public void btnNext() {

		btnNext.click();
	}
	
	public void entePassword() {

		password_txt.sendKeys("vTouch-12");
	}
	
	public void btnLogin() {

		btnLogin.click();
		
	}
	

	public void agree_continue_btn() {

		agree_continue_btn.click();
	}


	

	
	
	
	

}
