package com.ims.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SalesOrderPage {
	
	
	WebDriver ldriver;

	public SalesOrderPage(WebDriver rdriver) {
		ldriver = rdriver;
		PageFactory.initElements(rdriver, this);

	}
	
	@FindBy(css = "button.nav-arrow.right-direction-arrow > svg")
	@CacheLookup
	WebElement load_Page;
 
	@FindBy(css = "//body/div[@id='root']/div[1]/div[2]/div[1]/div[1]/ul[1]/li[4]/a[1]/span[1]")
	@CacheLookup
	WebElement load_SalesOrderModule;

	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[1]/button[1]")
	@CacheLookup
	WebElement Add_NewSalesOrder;

	@FindBy(xpath= ".css-1gtu0rj-indicatorContainer > .css-19bqh2r")
	@CacheLookup
	WebElement cust_List;
	
	
	
	 @FindBy(css = "//div[contains(text(),'Solutin')]")
		@CacheLookup
		WebElement select_customer;
	 

	@FindBy(xpath = "//tbody/tr[1]/td[1]/div[1]/div[1]/div[1]/div[2]/div[1]/*[1]")
	@CacheLookup
	WebElement getItemList;

	@FindBy(xpath = "//div[contains(text(),'Orange Watch')]")
	@CacheLookup
	WebElement selectItem;
	
	@FindBy(xpath = "//body/div[@id='root']/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/form[1]/div[1]/div[2]/div[9]/button[1]")
	@CacheLookup
	WebElement saveSO;

	
	
	public void load_Page() {

		load_Page.click();
		
	}
	
	public void load_SalesOrderModule() {

		load_SalesOrderModule.click();
		
	}
	public void clickNewSalesOrder_button() {
		Add_NewSalesOrder.click();

	}



	public void getcust_List() {

		cust_List.click();
	}

	public void clickselecCus() {

		select_customer.click();
	}


	public WebElement loadItemList() {

		return getItemList;
	}

	public void clickselectItem() {

		selectItem.click();
	}
	public void clicksaveSO() {

		saveSO.click();
	}

	

}
