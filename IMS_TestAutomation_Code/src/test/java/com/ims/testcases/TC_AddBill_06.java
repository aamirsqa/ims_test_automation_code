package com.ims.testcases;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.ims.pageObjects.AddBillPage;
import com.ims.pageObjects.LoginPage;

public class TC_AddBill_06 extends BaseClass {

	@Test(invocationCount = 1)
	public void addBillTest() throws Exception {

		logger.info(" Landing page loaded ");

		LoginPage lp = new LoginPage(driver);
		AddBillPage addbill = new AddBillPage(driver);

		logger.info(driver.getTitle());
		lp.clickSignInButton();
		logger.info("Signin button clicked");
		Thread.sleep(2000);

		lp.setUserName(username);
		logger.info("Entered Username");
		lp.setPassword(password);
		logger.info("Entered Password");
		lp.clickLoginButton();
		logger.info("Login button is clicked");

		Thread.sleep(5000);
		
		if (driver.getTitle().equals("Dashboard")) {

			logger.info("Landing page title " + driver.getTitle());
			addbill.clickexpandMenu_btn();
			Thread.sleep(2000);
			addbill.clickbill_MenuItem();
			Thread.sleep(2000);
			addbill.clicknew_Bill_btn();
			addbill.clicvendor_list();
			Thread.sleep(2000);
			addbill.clicselectVendor();
			addbill.clickitemsList();
			addbill.clickselectItem();
			addbill.clickSaveBill();
			Thread.sleep(5000);
			
			
			Assert.assertTrue(true);
			
			logger.info("Bill is added!");
			
			lp.clickorgIcon();
			Thread.sleep(5000);
			lp.clicklogout_btn();

		} else {
			captureScreen(driver, "TC_AddBill_06");
			Assert.assertTrue(false);
			logger.info("TC_AddBill is failed ");
		}
	}

}
