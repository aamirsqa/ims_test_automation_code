package com.ims.testcases;

import org.testng.annotations.Test;

import com.ims.pageObjects.AddContactPage;
import com.ims.pageObjects.LoginPage;
import com.ims.pageObjects.MainPage;

public class TC_AddCustomerContact_02 extends BaseClass {

	@Test
	public void addCustomer() throws Exception {

		logger.info(" Landing page loaded ");

		LoginPage lp = new LoginPage(driver);
		MainPage mp = new MainPage(driver);
		AddContactPage addcustomer = new AddContactPage(driver);
		logger.info(driver.getTitle());

		lp.clickSignInButton();
		logger.info("Signin button clicked");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		lp.setUserName(username);
		logger.info("Entered Username");
		lp.setPassword(password);
		logger.info("Entered Password");
		lp.clickLoginButton();
		logger.info("Login button is clicked");

		Thread.sleep(3000);
		mp.clickContactModule();
		Thread.sleep(2000);
		if (driver.getTitle().equals("Contact Details")) {

			logger.info("Contact page loads");
			addcustomer.clickAdd_NewContact_button();
			addcustomer.clickCustomer();
			addcustomer.getFirstName().sendKeys("AhmedAslam");
			addcustomer.clickSaveContact();

			String pageTitle = driver.getTitle();

			logger.info("New item added successfully!" + "User is moved to " + pageTitle+" Page");

		} else {
			logger.info("Contact Details page does not load");
		}
	}

}
