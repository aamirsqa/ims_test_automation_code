package com.ims.testcases;

import org.testng.annotations.Test;

import com.ims.pageObjects.AddItem;
import com.ims.pageObjects.LoginPage;
import com.ims.pageObjects.MainPage;

public class TC_AddItem_04 extends BaseClass {

	
	
	
	@Test
	public void loginTest() throws Exception {
			
			logger.info(" Landing page loaded ");

			LoginPage lp = new LoginPage(driver);
			MainPage mp=new MainPage(driver);
			AddItem ai= new AddItem(driver);
			logger.info(driver.getTitle());
		
			lp.clickSignInButton();
			logger.info("Signin button clicked");
			Thread.sleep(3000);

			lp.setUserName(username);
			logger.info("Entered Username");
			lp.setPassword(password);
			logger.info("Entered Password");
			lp.clickLoginButton();
			logger.info("Login button is clicked");

			mp.clickItemModule();
			Thread.sleep(3000);
			if	(driver.getTitle().equals("Item Details"))
			{
				
			logger.info("Add item page loads");
			
			ai.clickAddItemButton();
			ai.getItemName().sendKeys("Shirts");
			ai.getitem_Unit().sendKeys("Piece");
			ai.clickGenSKU();
			Thread.sleep(2000);
			ai.getSaleUnitPrice().sendKeys("120");
			ai.getpurchaseUnitPrice().sendKeys("150");
			ai.getopeningStockValue().sendKeys("2000");
			ai.clickSaveIteam();	
			String pageTitle= driver.getTitle();
			
			logger.info("New item added successfully!"+"User is moved to "+ pageTitle );
			
			}else {
			logger.info("Add item page does not load");}
		}
}
