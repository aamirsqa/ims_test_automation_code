package com.ims.testcases;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ims.pageObjects.AddOrgPage;
import com.ims.pageObjects.LoginPage;
import com.ims.pageObjects.MainPage;


public class TC_AddNewOrg_10 extends BaseClass {
	
	
	
	@Test
	public void AddNewOrg() throws Exception {

		logger.info(" Landing page loaded ");

		LoginPage lp = new LoginPage(driver);
		MainPage mp = new MainPage(driver);
		AddOrgPage op = new AddOrgPage(driver);
		JavascriptExecutor js = (JavascriptExecutor) driver;

		logger.info(driver.getTitle());
		lp.clickSignInButton();
		logger.info("Signin button clicked");
		Thread.sleep(2000);

		lp.setUserName(username);
		logger.info("Entered Username");
		lp.setPassword(password);
		logger.info("Entered Password");
		lp.clickLoginButton();
		logger.info("Login button is clicked");
		Thread.sleep(5000);

		if (driver.getTitle().equals("Dashboard")) {
			op.load_Page();
			Thread.sleep(3000);
			mp.clickorgModule();
			Thread.sleep(3000);
			logger.info("Org module loaded");
			op.clickAdd_NewOrg_button();
			Thread.sleep(2000);
			op.getorg_Name().sendKeys("Master-Live");
			op.clickload_OrgTypeList();
			Thread.sleep(3000);
			op.setOrgType();
			
			Thread.sleep(2000);
			op.clickfYearList();
			Thread.sleep(1000);
			op.clickfYear();
			Thread.sleep(1000);
			op.clickbaseCurrencyList();
			op.clicksetBaseCurrency();
			js.executeScript("arguments[0].scrollIntoView();", op.setphoneNumber());
			op.setphoneNumber().sendKeys("+1 (232) 342-3423");
			op.clicksaveOrg();
		
			logger.info("New Org added sucessfully!" + "User back to " + driver.getTitle() + " Page");
		} else {
			captureScreen(driver, "TC_AddNewOrg_10");
			Assert.assertTrue(false);
			logger.info("TC_AddNewOrg is failed ");
		}
	}



}
