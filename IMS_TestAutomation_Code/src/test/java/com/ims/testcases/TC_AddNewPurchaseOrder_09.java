package com.ims.testcases;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ims.pageObjects.LoginPage;
import com.ims.pageObjects.MainPage;
import com.ims.pageObjects.PurchaseOrderPage;


public class TC_AddNewPurchaseOrder_09 extends BaseClass {
	@Test
	public void AddNewPurchaseOrder() throws Exception {

		logger.info(" Landing page loaded ");

		LoginPage lp = new LoginPage(driver);
		MainPage mp = new MainPage(driver);
		PurchaseOrderPage po = new PurchaseOrderPage(driver);
		JavascriptExecutor js = (JavascriptExecutor) driver;

		logger.info(driver.getTitle());
		lp.clickSignInButton();
		logger.info("Signin button clicked");
		Thread.sleep(2000);

		lp.setUserName(username);
		logger.info("Entered Username");
		lp.setPassword(password);
		logger.info("Entered Password");
		lp.clickLoginButton();
		logger.info("Login button is clicked");
		Thread.sleep(5000);

		if (driver.getTitle().equals("Dashboard")) {
			po.load_Page();
			Thread.sleep(3000);
			mp.clickpurchaseOrderModule();
			Thread.sleep(3000);
			logger.info("PurchaseOrder module loaded");
			po.clickNewPurchaseOrder_button();
			Thread.sleep(2000);
			po.getven_List();
			Thread.sleep(3000);
			po.clickSelectVendor();
			js.executeScript("arguments[0].scrollIntoView();", po.loadItemList());
			Thread.sleep(3000);
			po.loadItemList().click();
			Thread.sleep(1000);
			po.clickselectItem();
			Thread.sleep(2000);
			po.clicksavePO();
			logger.info("PurchaseOrder generated sucessfully!" + "User back to " + driver.getTitle() + " Page");
		} else {
			captureScreen(driver, "TC_AddNewPurchaseOrder_09");
			Assert.assertTrue(false);
			logger.info("TC_AddNewPurchaseOrder is failed ");
		}
	}


}
