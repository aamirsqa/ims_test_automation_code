package com.ims.testcases;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.ims.pageObjects.LoginPage;
import com.ims.pageObjects.MainPage;
import com.ims.pageObjects.SalesOrderPage;

public class TC_AddNewSalesOrder_07 extends BaseClass {

	@Test
	public void AddNewSalesOrder() throws Exception {

		logger.info(" Landing page loaded ");

		LoginPage lp = new LoginPage(driver);
		MainPage mp = new MainPage(driver);
		SalesOrderPage so = new SalesOrderPage(driver);
		JavascriptExecutor js = (JavascriptExecutor) driver;

		logger.info(driver.getTitle());
		lp.clickSignInButton();
		logger.info("Signin button clicked");
		Thread.sleep(2000);

		lp.setUserName(username);
		logger.info("Entered Username");
		lp.setPassword(password);
		logger.info("Entered Password");
		lp.clickLoginButton();
		logger.info("Login button is clicked");
		Thread.sleep(5000);

		if (driver.getTitle().equals("Dashboard")) {
			so.load_Page();
			mp.clicksalesOrderModule();
			Thread.sleep(3000);
			logger.info("SalesOrder module loaded");
			so.clickNewSalesOrder_button();
			Thread.sleep(5000);
			so.getcust_List();
			Thread.sleep(3000);
			so.clickselecCus();
			js.executeScript("arguments[0].scrollIntoView();", so.loadItemList());
			Thread.sleep(3000);
			so.loadItemList().click();
			Thread.sleep(1000);
			so.clickselectItem();
			Thread.sleep(2000);
			so.clicksaveSO();
			logger.info("SalesOrder generated sucessfully!" + "User back to " + driver.getTitle() + " Page");
		} else {
			captureScreen(driver, "TC_AddNewSalesOrder_07");
			Assert.assertTrue(false);
			logger.info("TC_AddNewSalesOrder Testcase is failed ");
		}
	}

}
