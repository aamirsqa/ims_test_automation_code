package com.ims.testcases;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.ims.pageObjects.AddPackagePage;
import com.ims.pageObjects.LoginPage;
import com.ims.pageObjects.MainPage;

public class TC_AddPackage_08 extends BaseClass {

	@Test
	public void AddPackage() throws Exception {

		logger.info(" Landing page loaded ");

		LoginPage lp = new LoginPage(driver);
		MainPage mp = new MainPage(driver);
		AddPackagePage ap = new AddPackagePage(driver);

		logger.info(driver.getTitle());
		lp.clickSignInButton();
		logger.info("Signin button clicked");
		Thread.sleep(2000);

		lp.setUserName(username);
		logger.info("Entered Username");
		lp.setPassword(password);
		logger.info("Entered Password");
		lp.clickLoginButton();
		logger.info("Login button is clicked");
		Thread.sleep(5000);

		if (driver.getTitle().equals("Dashboard")) {
			ap.load_Page();
			mp.clickpackageModule();
			Thread.sleep(3000);
			logger.info("Package module loaded");
			ap.clickAdd_Package_Button();
			Thread.sleep(2000);
			ap.getsalesOrder_List();
			Thread.sleep(3000);
			ap.getsalesOrder_No();
			ap.clickpackageSave_button();
			Thread.sleep(3000);
			logger.info("Package generated sucessfully!" + "User back to " + driver.getTitle() + " Page");
		} else {
			captureScreen(driver, "TC_AddPackage_08");
			Assert.assertTrue(false);
			logger.info("TC_AddPackage Testcase is failed ");
		}
	}

}
