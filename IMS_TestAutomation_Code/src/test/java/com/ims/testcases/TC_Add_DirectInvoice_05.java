package com.ims.testcases;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ims.pageObjects.CreateInvoice;
import com.ims.pageObjects.LoginPage;
import com.ims.pageObjects.MainPage;

public class TC_Add_DirectInvoice_05 extends BaseClass {
	
	
	@Test
	public void CreateDirectInvoiceTest() throws Exception {
			
			logger.info(" Landing page loaded ");

			LoginPage lp = new LoginPage(driver);
			MainPage mp= new MainPage(driver);
			CreateInvoice ci=new CreateInvoice(driver);
			
			logger.info(driver.getTitle());
			lp.clickSignInButton();
			logger.info("Signin button clicked");
			Thread.sleep(3000);

			lp.setUserName(username);
			logger.info("Entered Username");
			lp.setPassword(password);
			logger.info("Entered Password");
			lp.clickLoginButton();
			logger.info("Login button is clicked");
			Thread.sleep(5000);
			
			if (driver.getTitle().equals("Dashboard")) {
				ci.click_load_Page();
				Thread.sleep(3000);
				//mp.clickItemModule();
				Thread.sleep(3000);
				ci.clickload_Invoice_Module();
			
				Thread.sleep(2000);
				mp.clickInvoceModule();
				Thread.sleep(2000);
				logger.info("Load Invoice module");
				ci.clickAddInvoceButton();
				Thread.sleep(2000);
				ci.clickcustomerList();
				Thread.sleep(2000);
				ci.clickselect_customer();
				ci.load_item_List();
				Thread.sleep(2000);
				ci.click_select_Item();
				ci.click_save_button();
			} else {
				captureScreen(driver, "TC_Add_DirectInvoice_05");
				Assert.assertTrue(false);
				logger.info("TC_CreateDirectInvoice_002 Testcase is failed ");
			}
		}

}
