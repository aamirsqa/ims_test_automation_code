package com.ims.testcases;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ims.pageObjects.LoginPage;

public class TC_Login_01 extends BaseClass {

	@Test(invocationCount = 1)
	public void loginTest() throws Exception {
			
			Thread.sleep(3000);
			logger.info(" Landing page loaded ");

			LoginPage lp = new LoginPage(driver);

			logger.info(driver.getTitle());
			lp.clickSignInButton();
			logger.info("Signin button clicked");
			Thread.sleep(3000);

			lp.setUserName(username);
			logger.info("Entered Username");
			lp.setPassword(password);
			logger.info("Entered Password");
			lp.clickLoginButton();
			logger.info("Login button is clicked");

			Thread.sleep(5000);
			if (driver.getTitle().equals("Dashboard")) {

				logger.info("Landing page title " + driver.getTitle());
				Assert.assertTrue(true);
				logger.info("Login Testcase is passed ");
				lp.clickorgIcon();
				Thread.sleep(5000);
				lp.clicklogout_btn();
				

			} else {
				captureScreen(driver, "TC_Login_01");
				Assert.assertTrue(false);
				logger.info("Login Testcase is failed ");
			}
		}

		

	

}
